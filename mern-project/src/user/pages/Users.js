import React from 'react';

import UsersList from '../components/UsersList';

const Users = () => {
  const USERS = [
    {
      id: 'u1',
      name: 'Jess',
      image: 'https://via.placeholder.com/500',
      places: 3,
    },
  ];

  return <UsersList items={USERS} />;
};

export default Users;
